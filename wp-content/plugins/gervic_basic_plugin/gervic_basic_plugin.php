<?php
/*
* Plugin Name: Gervic Basic Plugin
* Description: Just a Basic Plugin
* Author: Gervic
* Version: 1.0
*/

//Exit if accesssed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


final class Gervic_Basic_Plugin {

	/*
	*	Initiate methods
	*/
	public function __construct() {
		$this->includes();
		$this->init_hooks();
		$this->filters();
	}

	/*
	*	Includes Php Files
	* 	@version 1.0
	*/
	private function includes() {
		include_once( plugin_dir_path( __FILE__ ) . 'includes/functions.php');
	}

	/*
	*	Initiate WP Hooks
	*	@version 1.0
	*/
	private function init_hooks() {
		//register_activation_hook( __FILE__, array( $this, 'after_activation' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_before_admin_bar_render', array( $this, 'wp_let_see_what' ) );
		add_action( 'init', array( $this, 'post_type_book' ) );
		add_action( 'init', array( $this, 'register_taxonomy' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_custom_metabox' ) );
		add_action( 'save_post', array($this, 'GBP_meta_save') );
	}

	public function enqueue_scripts() {
		global $pagenow, $typenow;
		
		if ( ($pagenow == 'post.php' || $pagenow == 'post-new.php') && $typenow == 'book' ) {
			wp_enqueue_style( 'GPB-admin-css', plugins_url( 'css/admin.css', __FILE__ ) );
			wp_enqueue_script( 'GBP-book-js', plugins_url( 'js/books.js', __FILE__ ), array( 'jquery', 'jquery-ui-datepicker' ), '201504', true );
			wp_enqueue_style( 'jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );
			wp_enqueue_script( 'GBP-quicktags', plugins_url( 'js/GBP-quicktags.js', __FILE__ ), array( 'quicktags' ), '20150206', true );
		}
	}

	/*
	*	Initiate WP Filters
	*	@version 1.0
	*/
	public function filters() {
		add_filter(  'gettext',  array( $this, 'change_feature_image_header_title' )  );
		add_filter(  'ngettext',  array( $this, 'change_feature_image_header_title' )  );
	}

	/* 
	*	add facebook button.
	*	@version 1.0
	*/
	public function wp_let_see_what() {
		global $wp_admin_bar;
		
		$wp_admin_bar->add_menu(array(
			'id' => 'facebook',
			'title' => 'Facebook',
			'href' => 'https://www.facebook.com',
			'meta' => array('target' => '_blank')
		));
	}

	/*
	*	Custom post type Book.
	*	@version 1.0
	*/
	public function post_type_book($args)  {
		$labels = apply_filters('GBP_post_type_labels', array(
			'name' => 'Books',
			'singular_name' => 'Book',
			'add_new' => 'Add New Book',
			'add_new_item' => 'Add New Book',
			'edit' => 'Edit',
			'edit_item' => 'Edit Book',
			'new_item' => 'New Book',
			'search_term' => 'Search Books',
			'parent' => 'Parent Book',
			'not_found' => 'No Books found',
			'not_found_in_trash' => 'No Books in Trash'
		));

		$args = apply_filters('GBP_post_type_args', array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_icon' => 'dashicons-businessman',
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => 10,
			'supports' => ['title', 'thumbnail']
		));

		register_post_type( 'book', $args );
	}

	/*
	*	Taxonomy for Custom post type Book
	*	@version 1.0
	*/
	public function register_taxonomy() {
		$labels = array(
			'name' => 'Attributes',
			'singular_name' => 'Attribute',
			'search_items' => 'Search Attribute',
			'popular_items' => 'Popular Attributes',
			'all_items' => 'All Attributes',
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => 'Edit Attribute',
			'update_item' => 'Update Attribute',
			'add_new_item' => 'Add New Attribute',
			'new_item_name' => 'New Attribute Name',
			'seperate_items_with_commas' => 'Seperate Attributes with commas',
			'add_or_remove_items' => 'Add or Remove Attributes',
			'choose_from_most_used' => 'Choose from the most used Attributes',
			'not_found' => 'No Attributes found.',
			'menu_name' => 'Attributes'
		);

		$args = array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'update_content_callback' => '_update_post_term_count',
			'query_var' => true,
			'rewrite' => array('slug' => 'attribute')
		);

		register_taxonomy( 'attribute', 'book', $args );
	}

	/*
	*	Custom Meta Box for Custom Fields
	*	@version 1.0
	*/
	public function add_custom_metabox() {
		add_meta_box('GBP_meta', 'Book Listing', array($this, 'GBP_meta_callback'), 'book', 'normal', 'core');
	}

	public function GBP_meta_callback( $post ) {
		wp_nonce_field( basename( __FILE__ ), 'GBP_books_nonce' );
		$GBP_stored_meta = get_post_meta( get_the_ID() );
		?>

		<div>
			<div class="meta-row">
				<div class="meta-th">
					<label for="book-id" class="GBP-row-title">Book ID:</label>
				</div>
				<div class="meta-table-data">
					<input type="text" name="book_id" id="book-id" value="<?php if ( ! empty ( $GBP_stored_meta['book_id'] )) echo esc_attr( $GBP_stored_meta['book_id'][0] ) ?>" />
				</div>
			</div>
		</div>
		<div class="book-description">
			Book Description <?php echo get_post_type(); ?>
		</div>
		<div class="meta-editor">

			<?php
				$content = get_post_meta(get_the_ID(), 'preferred_requirements', true);
				$editor = 'preferred_requirements';
				$settings = array(
					'textarea_rows' => 10,
					'media_buttons' => true
				);

				wp_editor($content, $editor, $settings);
			?>
		</div>

		  <div class="meta-row">
		        <div class="meta-th">
		          <label for="minimum-requirements" class="wpdt-row-title"><?php _e( 'Minimum Requirements', 'hrm-textdomain' )?></label>
		        </div>
		        <div class="meta-td">
		          <textarea name="minimum-requirements" class ="GBP-textarea" id="minimum-requirements"><?php if ( isset ( $GBP_stored_meta['minimum-requirements'] ) ) echo esc_attr( $GBP_stored_meta['minimum-requirements'][0] ); ?></textarea>
		        </div>
		    </div>
		    <div class="meta-row">
		        <div class="meta-th">
		          <label for="preferred-requirements" class="wpdt-row-title"><?php _e( 'Preferred Requirements', 'hrm-textdomain' )?></label>
		        </div>
		        <div class="meta-td">
		          <textarea name="preferred-requirements" class ="GBP-textarea" id="preferred-requirements"><?php if ( isset ( $GBP_stored_meta['preferred-requirements'] ) ) echo esc_attr( $GBP_stored_meta['preferred-requirements'][0] ); ?></textarea>
		        </div>
		    </div>
		    <div class="meta-row">
		        <div class="meta-th">
		          <label for="relocation-assistance" class="prfx-row-title"><?php _e( 'Relocation Assistance', 'hrm-textdomain' )?></label>
		        </div>
		        <div class="meta-td">
		          <select name="relocation-assistance" id="relocation-assistance">
		              <option value="select-yes" <?php if ( isset ( $GBP_stored_meta['on-assistance'] ) == 'select-yes' ) echo 'selected' ?>>Yes</option>';
		              <option value="select-no" <?php if ( isset ( $GBP_stored_meta['on-assistance'] ) == 'select-no' ) echo 'selected' ?>>No</option>';
		          </select>
		    </div> 
		</div>  
		<?php
	}

	public function change_feature_image_header_title( $translated ) {
		$translated = str_ireplace(  'Featured Image',  'Slideshow Image',  $translated );
		return $translated;
	}

	/*
	*	Custom Field Validation and Submition
	*	@version 1.0
	*/
	public function GBP_meta_save( $post_id ) {
		//Check save status
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST['GBP_books_nonce'] ) && wp_verify_nonce( $_POST['GBP_books_nonce'], basename( __FILE__ ) ) ) ? true : false;

		if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
			return;
		}

		if ( isset ( $_POST['book_id'] ) ) {
			update_post_meta( $post_id, 'book_id', sanitize_text_field( $_POST['book_id'] ) );
		}

		if ( isset ($_POST['minimum-requirements']) ) {
			update_post_meta( $post_id, 'minimum-requirements', sanitize_text_field( $_POST['minimum-requirements'] ) );
		}

		if ( isset ( $_POST['preferred_requirements'] ) ) {
			update_post_meta( $post_id, 'preferred_requirements', sanitize_text_field( $_POST['preferred_requirements'] ) );
		}

		if ( isset ( $_POST['on-assistance'] ) ) {
			update_post_meta( $post_id, 'on-assistance', sanitize_text_field( $_POST['on-assistance'] ) );
		}
	}
}

function GBP() {
	return new Gervic_Basic_Plugin();
}

$GLOBALS['Gervic_Basic_Plugin'] = GBP();