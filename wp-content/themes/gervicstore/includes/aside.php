<div class="aside-container">
	<ul>
		<li>
			<div class="aside-search-container">
				<form action="/" method="get" accept-charset="UTF-8">
					<input type="text" name="s" class="search-text-field" autocomplete="off" />
					<input type="hidden" name="post_type" value="product" />
					<button type="submit" class="search-button" placeholder="Search here...">Search</button>
				</form>
			</div>
		</li>
		<?php if (is_active_sidebar('aside-right')): ?>
			<?php dynamic_sidebar('aside-right'); ?>
		<?php else: ?>
			<li>&nbsp;</li>
		<?php endif; ?>
	</ul>
</div>