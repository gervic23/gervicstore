<div class="homepage-container">
	<?php $loop = woocommerce_fetch_whats_hot_items(); if ($loop->have_posts()): ?>
		<div class="homepage-whatshot-container">
			<div class="homepage-whatshot-header">What's Hot</div>
			<div class="homepage-whatshot-contents">
				<ul>
					<?php while ($loop->have_posts()): $loop->the_post(); global $product; ?>
						<li>
							<div class="homepage-product-img">
								<a href="<?php the_permalink(); ?>"><?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="65px" height="115px" />'; ?></a>
							</div>
							<?php
								$title = get_the_title();
								$length = strlen($title);

								if ($length >= 20) {
									$new_title = substr($title, 0, 19).'...';
								} else {
									$new_title = $title;
								}
							?>
							<div class="homepage-product-name"><a href="<?php the_permalink() ?>"><?php echo $new_title; ?></a></div>
							<div class="homepage-product-price"><?php echo $product->get_price_html(); ?></div>
							<div class="homepage-product-cart">
								<a href="<?php the_permalink(); ?>">View Details</a>
								<a href="<?php the_permalink(); ?>?add-to-cart=<?php echo $loop->post->ID; ?>">Add To Cart</a>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
				<div class="clear"></div>
				<div class="homepage-product-showmore">
					<a href="/product-category/whats-hot/">Show More</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php $loop = woocommerce_fetch_new_added_items(); if ($loop->have_posts()): ?>
		<div class="homepage-new-items-container">
			<div class="homepage-new-items-header">New Items</div>
			<div class="homepage-new-items-contents">
				<ul>
					<?php while ($loop->have_posts()): $loop->the_post(); global $product; ?>
						<li>
							<div class="homepage-product-img">
								<a href="<?php the_permalink() ?>"><?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="65px" height="115px" />'; ?></a>
							</div>
							<?php
								$title = get_the_title();
								$length = strlen($title);

								if ($length >= 20) {
									$new_title = substr($title, 0, 19).'...';
								} else {
									$new_title = $title;
								}
							?>
							<div class="homepage-product-name"><a href="<?php the_permalink() ?>"><?php echo $new_title; ?></a></div>
							<div class="homepage-product-price"><?php echo $product->get_price_html(); ?></div>
							<div class="homepage-product-cart">
								<a href="<?php the_permalink(); ?>">View Details</a>
								<a href="<?php the_permalink(); ?>?add-to-cart=<?php echo $loop->post->ID; ?>">Add To Cart</a>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
				<div class="clear"></div>
				<div class="homepage-product-showmore">
					<a href="/shop/">Show More</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>