<!-- Start WP Header -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link href='https://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body>

<div class="mainContainer">
	<div class="main-blackscreen"></div>
	<div class="cookie-notification">
		<div class="container">
			<div class="header">GervicStore Notification</div>
			<div class="content">
				<p>
					This is a demo online store created by Gervic as part of portfolio.<br />
					You may test buy in this webstore by using this Paypal Sandbox account.<br />
					Username: aeris-buyer@gmail.com 
					<br />Password: lightning
				</p>
			</div>
			<div class="button"><span>OK</span></div>
		</div>
	</div>
	<header class="mainHeader">
		<div class="topHeader">
			<div class="topHeader-left">
				<?php if (has_nav_menu('primary')): ?>
					<?php wp_nav_menu(array( 'theme_location' => 'primary' )); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
				<div class="clear"></div>
			</div>
			<div class="topHeader-right">
				<?php if (is_user_logged_in()): ?>
					<?php if (has_nav_menu('user-panel')): ?>
						<?php wp_nav_menu(array( 'theme_location' => 'user-panel' )); ?>
					<?php else: ?>
						aaa
					<?php endif; ?>
				<?php else: ?>
					<?php if (has_nav_menu('user-panel-unlogged')): ?>
						<?php wp_nav_menu(array( 'theme_location' => 'user-panel-unlogged' )); ?>
					<?php else: ?>
						&nbsp;
					<?php endif; ?>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>
		<div class="middleHeader">
			<a href="/"><img src="/wp-content/themes/gervicstore/img/logo.png" /></a>
		</div>
		<nav class="menuHeader">
			<div class="menuHeader-left">
				<?php if (has_nav_menu('bottom-header')): ?>
					<?php wp_nav_menu(array( 'theme_location' => 'bottom-header' )); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
				<div class="clear"></div>
			</div>
			<div class="menuHeader-right">
				<div class="header-cart-container">
					<span class="icon-cart"></span>
					<span class="items-count">Items Count: <span>0</span></span>
					<span class="cart-menu-dropdown">▼</span>
				</div>
			</div>
			<div class="clear"></div>
			
		</nav>
		<div class="clear"></div>
	</header>

	<header class="mainHeaderFixed">
		<div class="mainHeaderFixed-top">
			<div class="mainHeaderFixed-top-left">
				<?php if (has_nav_menu('categories-fixed')): ?>
					<?php wp_nav_menu( array( 'theme_location' => 'categories-fixed' ) ); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
				<div class="clear"></div>
			</div>
			<div class="mainHeaderFixed-top-right">
				<?php if (is_user_logged_in()): ?>
					<?php if (has_nav_menu('user-panel-fixed') ): ?>
						<ul>
							<li>
								<span class="header-fixed-cart-container">
									<span class="icon-cart"></span>
									<span class="mainHeaderFixed-item-count">0</span>
									<span class="cart-menu-dropdown">▼</span>
								</span>
							</li>
						</ul>
						<?php wp_nav_menu( array( 'theme_location' => 'user-panel-fixed' ) ); ?>
					<?php else: ?>
						<span class="header-fixed-cart-container">
							<span class="icon-cart"></span>
							<span class="mainHeaderFixed-item-count">0</span>
							<span class="cart-menu-dropdown">▼</span>
						</span>
					<?php endif; ?>
					<div class="clear"></div>
				<?php else: ?>
					<?php if (has_nav_menu('user-panel-fixed-unlogged')): ?>
						<ul>
							<li>
								<span class="header-fixed-cart-container">
									<span class="icon-cart"></span>
									<span class="mainHeaderFixed-item-count">0</span>
									<span class="cart-menu-dropdown">▼</span>
								</span>
							</li>
						</ul>
						<?php wp_nav_menu( array( 'theme_location' => 'user-panel-fixed-unlogged' ) ); ?>
					<?php else: ?>
						<ul>
							<li>
								<span class="header-fixed-cart-container">
									<span class="icon-cart"></span>
									<span class="mainHeaderFixed-item-count">0</span>
									<span class="cart-menu-dropdown">▼</span>
								</span>
							</li>
						</ul>
					<?php endif; ?>
					<div class="clear"></div>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>
		<div class="mainHeaderFixed-bottom">
			<span>
				<?php if (has_nav_menu('menu-floating')): ?>
					<?php wp_nav_menu( array( 'theme_location' => 'menu-floating' ) ); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			</span>
			<div class="clear"></div>
		</div>
	</header>

	<div class="header-cart-list">
		
	</div>

	<header class="mobileHeader">
		<ul>
			<li><span class="glyph-icons menu-icon mobile-menu-button"></span></li>
			<li>
				<form action="/" method="get" accept-charset="UTF-8">
					<input type="text" name="s" placeholder="Search Here..." autocomplete="off" />
					<input type="hidden" name="post_type" value="product" />
				</form>
			</li>
			<li><span class="glyph-icons search-icon mobile-search-button"></span></li>
		</ul>
	</header>

	<div class="mobile-nav-container">
		<div class="m-user-account">
			<?php if (is_user_logged_in()): ?>
				<?php if (has_nav_menu('mobile-user-panel-logged-in')): ?>
					<?php wp_nav_menu(array( 'theme_location' => 'mobile-user-panel-logged-in' )); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			<?php else: ?>
				<?php if (has_nav_menu('user-panel-unlogged')): ?>
					<?php wp_nav_menu(array( 'theme_location' => 'user-panel-unlogged')); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<div class="top">
			<div class="left">
				<a href="/"><img alt="m-logo" src="/wp-content/themes/gervicstore/img/logo.png" /></a>
			</div>
			<div class="right">
				<span class="m-cart-container">
					<a href="/cart/"><span class="icon-cart m-cart-button"></span> Items Count: <span class="m-items-count">0</span></a>
				</span>
			</div>
			<div class="clear"></div>
		</div>
		<div class="middle">
			<div class="left">
				<h2>Products</h2>
			</div>
			<div class="right">
				<h2>Pages</h2>
			</div>
			<div class="clear"></div>
		</div>
		<div class="bottom">
			<div class="left">
				<?php if (has_nav_menu('bottom-header')): ?>
					<?php wp_nav_menu(array( 'theme_location' => 'bottom-header' )); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			</div>
			<div class="right">
				<?php if (has_nav_menu('primary')): ?>
					<?php wp_nav_menu(array( 'theme_location' => 'primary' )); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<!-- Javascript for Header -->
	<script type="text/javascript">
		var $ = jQuery;

		<?php if (is_user_logged_in() && is_super_admin()): ?>
			$(".mainHeaderFixed").css({"top": 31 + "px"});
		<?php else: ?>
			$(".mainHeaderFixed").css({"top": 0 + "px"});
		<?php endif; ?>
	</script>

	<script type="text/javascript">
		var $ = jQuery;
		var win_height = $(window).height();
		var win_width = $(window).width();
		var mainHeaderOuterHeight = $(".mainHeader").outerHeight(true);
		var header_cart_toggle = false;
		/*
		$(window).on("contextmenu", function() {
			alert("Rightclick disabled");
			return false;
		});

		$(window).on("cut copy paste", function(e) {
			e.preventDefault();
		}); 
		*/
		<?php if (is_user_logged_in() && is_super_admin()): ?>
			$(".header-cart-list").css({
				"top": mainHeaderOuterHeight + 30 + "px"
			});
		<?php else: ?>
			$(".header-cart-list").css({
				"top": mainHeaderOuterHeight + 0 + "px"
			});
		<?php endif; ?>

		$(window).on("scroll", function() {
			<?php if (is_user_logged_in() && is_super_admin()): ?>
				var fixed_header_height = $(".mainHeaderFixed-bottom").outerHeight(true) + 68;
			<?php else: ?>
				var fixed_header_height = $(".mainHeaderFixed-bottom").outerHeight(true) + 40;
			<?php endif; ?>

			if ($(window).scrollTop() >= mainHeaderOuterHeight) {
				$(".mainHeader").css({"visibility": "hidden"});
				$(".mainHeaderFixed").show();

				if (win_width > 800) {
					setTimeout(function() {
						$(".mainHeaderFixed-bottom").slideDown(500);
					}, 500);
				}

				$(".header-cart-list").css({
					"position": "fixed",
					"top": fixed_header_height + "px"
				});

			} else {
				$(".mainHeaderFixed").hide();
				$(".mainHeaderFixed-bottom").hide();
				$(".mainHeader").css({"visibility": "visible"});

				<?php if (is_user_logged_in() && is_super_admin()): ?>
					$(".header-cart-list").css({
						"position": "absolute",
						"top": mainHeaderOuterHeight + 30 + "px",
					});
				<?php else: ?>
					$(".header-cart-list").css({
						"position": "absolute",
						"top": mainHeaderOuterHeight + 0 + "px",
					});
				<?php endif; ?>
			}
		});

		ajax_cart();

		if (document.location.toString().toLowerCase().split("/")[3] !== "cart" && document.location.toString().toLowerCase().split("/")[3] !== "checkout") {
			$(".header-cart-container").on("click", function() {
				if (header_cart_toggle == false) {
					header_cart_toggle = true;
					$(".header-cart-list").fadeIn(500, function() {
						var h = $(".header-cart-list").outerHeight(true) - 70;

						$(".header-cart-list-top").slimScroll({
							height: h + "px",
							color: "#033d74"
						});
					});
				} else {
					header_cart_toggle = false;
					$(".header-cart-list").fadeOut(500);
				}
			});

			$(".header-fixed-cart-container").on("click", function() {
				if (header_cart_toggle == false) {
					header_cart_toggle = true;
					$(".header-cart-list").fadeIn(500, function() {
						var h = $(".header-cart-list").outerHeight(true) - 70;

						$(".header-cart-list-top").slimScroll({
							height: h + "px",
							color: "#033d74"
						});
					});

					$(".header-cart-list").css({
						"position": "fixed",
						"top": fixed_header_height + "px"
					});
				} else {
					header_cart_toggle = false;
					$(".header-cart-list").fadeOut(500);
				}
			});
		}

		var item_count_data = {
			"action": "my_action",
			"operation": "item_count"
		};

		$.post("/wp-admin/admin-ajax.php", item_count_data, function(r) {
			$(".items-count span, .m-items-count").html(r);
			$(".mainHeaderFixed-item-count").html(r);
		});

		function ajax_cart() {
			var cart_data = {
				"action": "my_action",
				"operation": "cart-list"
			};

			$.post("/wp-admin/admin-ajax.php", cart_data, function(r) {
				$(".header-cart-list").html(r);
			});
		}

		$(".mobile-search-button").on("click", function() {
			$(".mobileHeader form").submit();
		});

		<?php if (is_user_logged_in() && is_super_admin()): ?>
			mobile_resize();

			$(window).resize(function() {
				mobile_resize();
			});

			function mobile_resize() {
				var win_height = $(window).height();
				var win_width = $(window).width();

				if (win_width <= 800) {
					if (win_width >= 782) {
						var nav_height = win_height - 83;
						var border_height = win_height - 221;

						$(".mobileHeader").css({
							"top": 32 + "px"
						});

						$(".mobile-nav-container").css({
							"top": 81 + "px",
							"height": nav_height + "px"
						});

						$(".mobile-nav-container .bottom .left, .mobile-nav-container .bottom .right").css({
							"height": border_height + "px"
						});
					} else if (win_width < 782) {
						var nav_height = win_height - 97;
						var border_height = win_height - 232;

						$(".mobileHeader").css({
							"top": 45 + "px"
						});

						$(".mobile-nav-container").css({
							"top": 95 + "px",
							"height": nav_height + "px"
						});

						$(".mobile-nav-container .bottom .left, .mobile-nav-container .bottom .right").css({
							"height": border_height + "px"
						});
					}
				}
			}
		<?php else: ?>
			mobile_resize();

			$(window).resize(function() {
				mobile_resize();
			});

			function mobile_resize() {
				var win_height = $(window).height();
				var win_width = $(window).width();

				var nav_height = win_height - 52;
				var border_height = win_height - 185;

				$(".mobile-nav-container").css({
					"height": nav_height + "px"
				});

				$(".mobile-nav-container .bottom .left, .mobile-nav-container .bottom .right").css({
					"height": border_height + "px",
				});
			}
		<?php endif; ?>

		var mobile_nav_toggle = false;

		$(".mobile-menu-button").on("click", function() {
			if (mobile_nav_toggle == false) {
				mobile_nav_toggle = true;
				$(".mobile-nav-container").animate({
					left: 0
				}, 500);

				$(".mobileHeader ul li:nth-child(1)").rotate({
					duration: 500,
			      	angle: 0,
			      	animateTo: 360
				}, 500);
			} else {
				mobile_nav_toggle = false;
				$(".mobile-nav-container").animate({
					left: -101 + "%"
				}, 500);

				$(".mobileHeader ul li:nth-child(1)").rotate({
					duration: 500,
			      	angle: 0,
			      	animateTo: -360
				}, 500);
			}
		});

		<?php if (is_user_logged_in()): ?>
			$(".mobile-nav-container .m-user-account").css({
				"text-align": "center"
			});

			$(".mobile-nav-container .m-user-account ul li").css({
				"margin": 0 + "px " + 2 + "%"
			});
		<?php endif; ?>

		if (!getCookie()) {
			$(".main-blackscreen").show();
			$(".cookie-notification").show();
			setCookie();
		}

		$(".cookie-notification .button span, .main-blackscreen").on("click", function() {
			$(".main-blackscreen").fadeOut(500);
			$(".cookie-notification").fadeOut(500);
		});

		function setCookie() {
			var d = new Date();
			d.setTime(d.getTime() + ((60 * 1000) * 60));
			var expires = "expires=" + d.toUTCString();
			document.cookie = "showned=1;" + "expires=" + expires + ";" + "path=/"
		}

		function getCookie() {
			if (document.cookie != "") {
				var name = "showned=";
				var ca = document.cookie.split(";");

				if (ca.length == 1 && ca.length != 0) {
					for (var i = 0; i < ca.length; i++) {
						var c = ca[i];
						while (c.charAt(0) == "") {
							c.substring(1);
						}

						if (c.indexOf(name) == 0) {
							return c.substring(name.length, c.length);
						}
					}
				} else {
					for (var i = 0; i < ca.length; i++) {
						var c = ca[i];
						while (c.charAt(0) == "") {
							c.substring(1);
						}

						if (c.indexOf(name) == 1) {
							return c.substring(name.length + 1, c.length);
						}
					}
				}
			} else {
				return "";
			}
		}
	</script>