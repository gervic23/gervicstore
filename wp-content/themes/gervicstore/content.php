<div class="mainSection">
	<div class="mainWpContent">
		<div class="mybreadcrumb">
			<span><a href="/">Home</a></span><span>Blog</span>
		</div>
		<?php if (have_posts()): ?>
			<?php while (have_posts()): the_post(); ?>
				<?php
					$categories = get_the_category();
					$output = '';

					if ($categories) {
						foreach ($categories as $category) {
							$output .= '<a href="'.get_category_link($category->term_id).'">'.$category->cat_name.'</a>, ';
						}

						$output = rtrim($output, ', ');
					}
				?>

				<article class="mainArticle">
					<div class="articleLeft">
						<div class="articleThumbnail">
							<?php if (has_post_thumbnail()): ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="articleRight">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<div class="articleInfo">
							<ul>
								<li>
									Posted By: <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a>
								</li>
								<li>Date: <?php the_time('F j Y g:i:a') ?></li>
								<li>Category: <?php echo $output; ?></li>
							</ul>
						</div>
						<div class="articleContent">
							<?php the_excerpt(); ?>
						</div>
					</div>
					<div class="clear"></div>
				</article>
			<?php endwhile; ?>
			<div> <?php pagination(); ?></div>
		<?php endif; ?>
	</div>
</div>

<aside class="mainAside">
	<?php require './wp-content/themes/gervicstore/includes/aside.php'; ?>
</aside>

<div class="clear"></div>