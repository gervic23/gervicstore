<?php get_header(); ?>

<section class="mainSection">
	<?php if (have_posts()): ?>
		<?php while (have_posts()): the_post(); ?>
			<div class="articleMinContent">
				<?php the_content(); ?>
			</div>
		<?php endwhile; ?>
	<?php else: ?>
		&nbsp;
	<?php endif; ?>
</section>

<aside class="mainAside">
	<?php require './wp-content/themes/gervicstore/includes/aside.php'; ?>
</aside>

<div class="clear"></div>

<?php get_footer(); ?>