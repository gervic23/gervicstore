		<footer class="mainFooter">
			<div class="footer-top">
				<div class="footer-top-left">
					<div class="footer-top-left-container">
						<h2>Contact Information</h2>
						<div class="home-address">
							<div class="left">
								<span class="glyph-icons home-icon"></span>
							</div>
							<div class="right">
								1234 Barangay<br />
								Maligaya Novaliches Quezon City<br />
								Philippines
							</div>
							<div class="clear"></div>
						</div>
						<div class="contacts">
							<div class="left">
								<span class="glyph-icons device-icon"></span>
							</div>
							<div class="right">
								09991234567
							</div>
							<div class="clear"></div>
						</div>
						<div class="email">
							<div class="left">
								<span class="glyph-icons mail-icon"></span>
							</div>
							<div class="right">
								gervic@gmx.com
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<div class="footer-top-middle">
					<div class="footer-top-middle-container">
						<h2>Keep Connected</h2>
						<ul>
							<li>
								<span class="social-media-icons fb-icon"></span> Like us on Facebook
							</li>
							<li>
								<span class="social-media-icons twitter-icon"></span> Follow us on Twitter
							</li>
							<li>
								<span class="social-media-icons google-icon"></span> Add us on Google+
							</li>
							<li>
								<span class="social-media-icons linkedin-icon"></span> Follow us on Linkedin
							</li>
							<li>
								<span class="social-media-icons rss-icon"></span> Subscribe to our Feeds
							</li>
						</ul>
					</div>
				</div>
				<div class="footer-top-right">
					<div class="footer-top-right-container">
						<h2>Product Search</h2>
						<form class="footer-search-form" action="/" method="get" accept-charset="UTF-8">
							<input type="text" name="s" placeholder="Search Here..." autocomplete="off" />
							<input type="hidden" name="post_type" value="product" />
							<input type="submit" value="search" />
						</form>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="m-footer">
				<div class="m-footer-left">
					<div class="business-addr">
						<h2>Business Location</h2>
						<p>
							1234 Barangay<br />
							Maligaya Novaliches Quezon City<br />
							Philippines
						</p>
					</div>
					<div class="contacts">
						<h2>Contacts</h2>
						<p>
							Contact Number: 09267294114<br />
							Email: gervic@gmx.com
						</p>
					</div>
				</div>
				<div class="m-footer-right">
					<h2>Keep Connected</h2>
					<ul>
						<li><span class="social-media-icons fb-icon"></span></li>
						<li><span class="social-media-icons twitter-icon"></span></li>
						<li><span class="social-media-icons google-icon"></span></li>
						<li><span class="social-media-icons linkedin-icon"></span></li>
						<li><span class="social-media-icons rss-icon"></span></li>
					</ul>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="footer-bottom-left">
					&copy; <?php echo date('Y'); ?> Gervic Store WordPress Woocommerce Web Store<span></span>Created by Gervic
				</div>
				<div class="footer-bottom-right">
					<a href="/about-us">About Us</a><span></span><a href="#">Privacy Policy</a><span></span><a href="/terms-of-service">Terms of Service</a>
				</div>
				<div class="clear"></div>
			</div>
		</footer>
	</div>
	<?php if (is_user_logged_in() && is_super_admin()): ?>
		<?php wp_footer(); ?>
	<?php endif; ?>

	<script type="text/javascript" src="/wp-content/themes/gervicstore/js/viewport-checker-effect.js"></script>

	<script type="text/javascript">
		var $ = jQuery;

		$(".mainSection, #container").on("click", function() {
			header_cart_toggle = false;
			$(".header-cart-list").fadeOut(500);
		});
	</script>
</body>
</html>
