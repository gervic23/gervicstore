<?php get_header(); ?>

<section class="mainSection">
	<div class="mainWpContent">
		<?php if (have_posts()): ?>
			<?php while (have_posts()): the_post(); ?>
				<?php
					$categories = get_the_category();
					$output = '';

					if ($categories) {
						foreach ($categories as $category) {
							$output .= '<a href="'.get_category_link($category->term_id).'">'.$category->cat_name.'</a>, ';
						}

						$output = rtrim($output, ', ');
					}
				?>

				<div class="mybreadcrumb">
					<?php echo get_breadcrumb(); ?>
				</div>
				<div class="articleMinContent">
					<div class="singleThumbnail">
						<?php if (has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php endif; ?>
					</div>
					<h2><?php the_title(); ?></h2>
					<div class="singleInfo">
						<ul>
							<li>
								Posted By: <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a>
							</li>
							<li>Date: <?php the_time('F j Y g:i:a') ?></li>
							<li>Category: <?php echo $output; ?></li>
						</ul>
					</div>
					<div class="singleContents">
						<?php the_content(); ?>
					</div>
					<div class="articleComments">
						<?php comments_template('', true); ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php else: ?>
			&nbsp;
		<?php endif; ?>
	</div>
</section>

<script type="text/javascript">
	var $ = jQuery;

	var contents_width = $(".mainWpContent").width();
	var thumbnail_width = $(".singleThumbnail img").width();

	if (thumbnail_width >= contents_width) {
		$(".singleThumbnail img").css({
			"width": 100 + "%",
			"height": 100 + "%"
		});
	}
</script>

<aside class="mainAside">
	<?php require './wp-content/themes/gervicstore/includes/aside.php'; ?>
</aside>

<div class="clear"></div>

<?php get_footer(); ?>