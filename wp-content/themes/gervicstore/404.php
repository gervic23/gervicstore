<?php get_header(); ?>

<div class="mainSection">
	<div class="not-fount-container">
		<img alt="404" src="/wp-content/themes/gervicstore/img/404.png" />
	</div>
</div>

<aside class="mainAside">
	<?php require './wp-content/themes/gervicstore/includes/aside.php'; ?>
</aside>

<div class="clear"></div>

<?php get_footer(); ?>