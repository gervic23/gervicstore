<?php

wp_dequeue_script( 'wc-add-to-cart-variation' );
wp_dequeue_script( 'wc-cart' );

function wpincludes() {
	wp_enqueue_style('main-style', get_template_directory_uri().'/style.css');
	wp_enqueue_style('viewportchecker-animate-style', get_template_directory_uri().'/css/animate.css');
	wp_enqueue_style('viewportchecker-demo-style', get_template_directory_uri().'/css/demo.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-visible', get_template_directory_uri() . '/js/jquery.visible.min.js');
	wp_enqueue_script('jquery-rotate', get_template_directory_uri().'/js/jQueryRotate.js');
}
add_action('wp_enqueue_scripts', 'wpincludes');

function theme_setup() {
	add_theme_support( 'woocommerce' );

	add_theme_support( 'title-tag' );

	register_nav_menus(array(
		'primary' => __('Top Header Menu'),
		'user-panel' => __('Top Header User Panel'),
		'user-panel-unlogged' => __('Top Header User Panel Unlogged In'),
		'bottom-header' => __('Bottom Header Menu'),
		'categories-fixed' => __('Category Floating Toolbar'),
		'user-panel-fixed' => __('User Panel Fixed'),
		'user-panel-fixed-unlogged' => __('User Panel Fixed Unlogged In'),
		'menu-floating' => __('Menu Floating Toolbar'),
		'menu-footer' => __('Menu Footer'),
		'mobile-user-panel-logged-in' => __('Mobile User Panel Logged In')
	));

	add_theme_support('post-thumbnails');
	add_image_size('small-thumbnail', 180, 120, true);
	add_image_size('banner-image', 920, 210, array('left', 'top'));

	register_sidebar(array(
		'name' => 'Aside Right',
		'id' => 'aside-right'
	));
}
add_action('after_setup_theme', 'theme_setup');

function homepage_contents() {
	ob_start();
	require './wp-content/themes/gervicstore/includes/homepage.php';
	return ob_get_clean();
}
add_shortcode('homepage', 'homepage_contents');

function woocommerce_fetch_whats_hot_items() {
	$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 3, 'product_cat' => 'whats-hot', 'orderby' => 'id', 'order' => 'DESC' );
    $loop = new WP_Query( $args );

    return $loop;
}

function woocommerce_fetch_new_added_items() {
	$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 10, 'orderby' => 'id', 'order' => 'DESC' );
    $loop = new WP_Query( $args );

    return $loop;
}

function woocommerce_fetch_all_products() {
	$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 2, 'orderby' =>'id', 'order' => 'DESC', 'limit' => 12 );
    $loop = new WP_Query( $args );

    return $loop;
}

function ajaxprocess() {
	global $woocommerce;
	$operation = $_POST['operation'];

	switch ($operation) {
		case 'cart-list':
			$items = $woocommerce->cart->get_cart();

			if (count($items) !== 0) {
				$amount_total = 0;

				echo '<div class="header-cart-list-top">';
				echo '<table>';
				echo '<thead>';
				echo '<tr>';
				echo '<th>&nbsp;</th>';
				echo '<th>Item Name</th>';
				echo '<th>Qty</th>';
				echo '<th>Price</th>';
				echo '<th>Total</th>';
				echo '<th>&nbsp;</th>';
				echo '</tr>';
				echo '</thead>';
				echo '<tbody>';

				foreach ($items as $item => $value):
					$_product = $value['data']->post;
					$product_details = wc_get_product($value['product_id']);
					$price = get_post_meta($value['product_id'] , '_price', true);
					$total_item_price = $price * $value['quantity'];
					$amount_total += $value['line_subtotal'];
				?>
					<tr style="border: 1px solid red" id="prod_id_<?php echo $value['product_id']; ?>">
						<td cellpadding="20"><a href="<?php echo $value['data']->post->guid; ?>"><?php echo $product_details->get_image(); ?></a></td>
						<td><a href="<?php echo $value['data']->post->guid; ?>"><?php echo $_product->post_title; ?></a></td>
						<td><?php echo $value['quantity'];?></td>
						<td>$<?php echo number_format($price, 2); ?></td>
						<td>$<?php echo number_format($value['line_subtotal'], 2); ?></td>
						<td id="totalid_<?php echo $value['product_id']; ?>"><img class="header_cart_del" id="item_<?php echo $value['product_id']; ?>" alt="close" src="/wp-content/themes/gervicstore/img/x.png" /></td>
					</tr>
				<?php
				endforeach;
				?>
					<tr>
						<td class="table-total-price" colspan="6">Total Price: $<?php echo number_format($amount_total, 2); ?></td>
					</tr>
				<?php
				echo '</tbody>';
				echo '</table>';
				echo '</div>';
				?>
					<div class="header-cart-list-bottom">
						<a href="/cart/">Go To Cart Page</a> <a href="/checkout/">Checkout</a>
					</div>

					<script type="text/javascript">
					var $ = jQuery;
					var h = $(".header-cart-list").outerHeight(true) - 70;

					$(".header-cart-list-top").slimScroll({
						height: h + "px",
						color: "#033d74"
					});

					$(".header_cart_del").each(function() {
						var id = $(this).attr("id").split("_")[1];
						$(this).on("click", function() {
							var data = {
								"action": "my_action",
								"operation": "del_cart_item",
								"id": id
							};

							$.post("/wp-admin/admin-ajax.php", data, function() {
								$(".header-cart-list-top table tbody #prod_id_" + id).fadeOut(500, function() {
									var count_data = {
										"action": "my_action",
										"operation": "item_count"
									};

									$.post("/wp-admin/admin-ajax.php", count_data, function(r) {
										$(".items-count").html("Items Count: " + r);
										$(".mainHeaderFixed-item-count").html(r);
									});

									ajax_cart();
								});
							});
						});
					});
					</script>
				<?php
			} else {
				?>

				<div class="header-cart-list-empty">
					<img alt="empty-cart" src="/wp-content/themes/gervicstore/img/empty-cart.png" />
				</div>

				<?php
			}
		break;

		case 'item_count':
			$items = $woocommerce->cart->get_cart();
			echo count($items);
		break;

		case 'del_cart_item':
			$items = $woocommerce->cart->get_cart();
			$id = $_POST['id'];

			foreach ($items as $item => $value) {
				if ($value['product_id'] == $id) {
					$woocommerce->cart->remove_cart_item($item);
				}
			}
		break;
	}

	wp_die();
}

add_action('wp_ajax_my_action', 'ajaxprocess');
add_action('wp_ajax_nopriv_my_action', 'ajaxprocess');

function wp_custom_excerpt($length) {
	return 20;
}
add_filter( 'excerpt_length', 'wp_custom_excerpt', 999 );

function wp_more_excerpt($more) {
	return sprintf( '...<div class="readMore"><a class="read-more" href="%1$s">%2$s</a></div>',
        get_permalink( get_the_ID() ),
        __( 'Read More', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wp_more_excerpt' );

function get_breadcrumb() {
	global $wpdb;
	global $portfolio_table;
	global $post;

	$trail = '<span><a href="/blog/">Blog</a></span>';
	$page_title = get_the_title($post->ID);

	if($post->post_parent) {
		$parent_id = $post->post_parent;


		while ($parent_id) {
			$page = get_page($parent_id);
			$breadcrumbs[] = '<span><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></span>';
			$parent_id = $page->post_parent;
		}

		$breadcrumbs = array_reverse($breadcrumbs);
		foreach($breadcrumbs as $crumb) $trail .= $crumb;
	}

	$trail .= '<span>'.$page_title.'</span>';
	$trail .= '';

	return '<span><a href="/">Home</a></span>'.$trail;
}

//remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

add_action( 'init', 'codex_custom_init' );
function codex_custom_init() {
    $args = [
      'public' => true,
      'label'  => 'Books',
      'supports' => ['title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields']
    ];
    register_post_type( 'book', $args );
}


class SidebarLogin extends WP_Widget {
	public function SidebarLogin() {
		parent::WP_Widget(false, $name = __('GervicStore Sidebar Login', 'sidebar-login'));
	}

	public function form($instance) {
		if ($instance) {
			$title = esc_attr($instance['title']);
		} else {
			$title = '';
		}

		?>

		<label for="<?php echo $this->get_field_id('title'); ?>">Header Title:</label><br />
		<input type="text" class="widget_input_title" style="width: 100%" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" /><br />

		<?php
	}

	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('Widget_title', $instance['title']);

		echo $before_widget;

		if ($title && !is_user_logged_in()) {
			echo $before_title.''.$title.''.$after_title;
		}


		?>
			<?php if (!is_user_logged_in()): ?>
				<ul>
				<div class="sidebar-login">
					<li>
						<form action="/my-account/" method="post" accept-charset="UTF-8">
							<p>
								<label for="username">Username:</label><br />
								<input type="text" name="username" id="sidebar-username" autocomplete="off" />
							</p>

							<p>
								<label for="password">Password:</label><br />
								<input type="password" name="password" id="sidebar-password"/>
							</p>

							<p>
								<label for="rememberme">Remember Me</label> &nbsp;
								<input type="checkbox" value="forever" />
							</p>

							<?php do_action( 'woocommerce_login_form' ); ?>

							<p>
								<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
								<input type="submit" name="login" value="Login" id="sidebar-rememberme" />
							</p>

							<p><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a></p>

							<?php do_action( 'woocommerce_login_form_end' ); ?>
						</form>
					</li>
				</div>
				</ul>
			<?php endif; ?>
		<?php

		echo $after_widget;
	}

	public function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
}
add_action('widgets_init', create_function('', 'return register_widget("SidebarLogin");'));

function pagination($pages = '', $range = 4) {
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}
