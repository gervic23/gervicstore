var $ = jQuery;
// $(document).ready(function() {
// 	$(".homepage-new-items-contents ul li, .products li").addClass("hidden").viewportChecker({
// 	    classToAdd: 'visible animated fadeIn',
// 	    offset: 100
// 	});
// });


$(".homepage-new-items-contents ul li, .products li").addClass("hidden");

$(".homepage-new-items-contents ul li").each(function(i) {

	if ($(this).visible()) {
		$(this).addClass("visible").addClass("animated").addClass("fadeIn");
	}
});

$(".products li").each(function() {
	$(this).addClass("hidden");

	if ($(this).visible()) {
		$(this).addClass("visible").addClass("animated").addClass("fadeIn");
	}
});

$(window).on("scroll", function() {
	$(".homepage-new-items-contents ul li").each(function(i) {

		if ($(this).visible()) {
			$(this).addClass("visible").addClass("animated").addClass("fadeIn");
		}
	});

	$(".products li").each(function() {
		$(this).addClass("hidden");

		if ($(this).visible()) {
			$(this).addClass("visible").addClass("animated").addClass("fadeIn");
		}
	});
});
